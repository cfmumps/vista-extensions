<cfcomponent access="remote" output="false">
	<cfset this.UUID = "">
	<cfset this.success = false>
	<cfset this.errorMessage = "">

	<cffunction name="login" returntype="component" access="remote" output="false">
		<cfargument name="accessCode" type="string" required="true">
		<cfargument name="verifyCode" type="string" required="true">

		<cfset this.UUID = createUUID()>
		<cfset var db = createObject("lib.cfmumps.mumps").open()>
		<cfset var result = db.mFunction("login^%cfmumpsVistaAPI", [this.UUID, accessCode, verifyCode])>

		<cfif result EQ "">
			<cfset this.success = true>
		<cfelse>
			<cfset this.success = false>
			<cfset this.errorMessage = result>
		</cfif>

		<cfset var glob = createObject("lib.cfmumps.global").open("KBBMCIDT", ["SESSION", this.UUID])>
		<cfset this.attributes = glob.getObject()>

		<cfset session.vistaSession = this>

		<cfreturn this>
	</cffunction>
</cfcomponent>
