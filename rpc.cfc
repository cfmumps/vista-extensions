component output=false extends=lib.cfmumps.util {

	public component function open(required string rpcName, required struct rpcContext)
	{
		this.name = arguments.rpcName;

		var validation = this.validateContextStruct(arguments.rpcContext);

		if(!validation.success) {
			throw(message = "Invalid RPC context", detail = validation.message);

			return(this);
		}
		else {
			this.rpcContext = arguments.rpcContext;
		}

		var db = createObject("lib.cfmumps.mumps");
		db.open();

		this.rpcIEN = db.order("XWB", [8994, "B", this.name, ""]).value;
		this.rpcRecord = db.get("XWB", [8994, this.rpcIEN, 0]);

		db.close();

		return(this);
	}

	public struct function call(required array inputParameters)
	{

	}

	private struct function validateContextStruct(required struct rpcContext)
	{
		var requiredKeys = "context,version,duz,division,use";
		var req = listToArray(requiredKeys);
		var retVal = {
			success: true,
			message: ""
		};


		for(key in req) {
			if(!structKeyExists(rpcContext, key)) {
				retval.message &= "'" & key & "' is required but was not provided." & chr(13) & chr(10);
				retval.success = false;
			}
		}

		return retVal;
	}
}