<cfcomponent output="false" extends="lib.cfmumps.util">
	<cfset this.fieldNumber = "">
	<cfset this.fieldLabel = "">

	<cfset this.attributes = {
	       multiple: false,
	       subfileNumber: false,
	       auditAlways: false,
	       auditEditOrDelete: false,
	       newSubentryVerification: true,
	       boolean: false,
	       computed: false,
	       multiLine: false,
	       dateValued: false,
	       freeText: false,
	       immutable: false,
	       printLength: false,
	       printWithDecimals: false,
	       mCode: false,
	       askAnotherSubentry: false,
	       numeric: false,
	       hasOutputTransform: false,
	       pointer: false,
	       pointerTo: "",
	       laygoAllowed: true,
	       entryRequired: false,
	       setOfCodes: false,
	       variablePointer: false,
	       wordProcessing: false,
	       wpLineMode: false,
	       allowDiModifyEdit: true,
	       wpWindowProcessing: true,
	       screenAssociated: false }>

	<cfset this.typeNames = {
	       string: 'Free Text',
	       enum: 'Set of Codes',
	       numeric: 'Numeric',
	       pointer: 'Pointer',
	       date: 'Date/Time',
	       text: 'Word Processing',
	       varpointer: 'Variable Pointer',
	       computed: 'Computed',
	       mumps: 'MUMPS Code'}>

	<cffunction name="open" returntype="component" access="public" output="false">
		<cfargument name="file" type="component" required="true">
		<cfargument name="fieldNumber" type="numeric" required="true">

		<cfset this.file = arguments.file>

		<cfset var global = "DD">
		<cfset var fieldDefSub = [file.ddNumber, arguments.fieldNumber, 0]>
		<cfset var outputTransformSub = [file.ddNumber, arguments.fieldNumber, 2]>

		<cfset var db = createObject("component", "lib.cfmumps.mumps")>
		<cfset db.open()>

		<cfset var fieldDef = db.get(global, fieldDefSub)>
		<cfset this.fieldDef = fieldDef>
		<cfset this.fieldNumber = val(arguments.fieldNumber)>
		<cfset this.fieldLabel = this.getPiece(fieldDef, "^", 1)>
		<cfset this.typeString = this.getPiece(fieldDef, "^", 2)>
		<cfset this.storageLocation = this.getPiece(fieldDef, "^", 4)>
		<cfset this.inputTransform = this.getPiece(fieldDef, "^", 5)>

		<cfset var oxfDef = db.data(global, outputTransformSub)>
		<cfif oxfDef.defined>
			<cfset this.outputTransform = db.get(global, outputTransformSub)>
		</cfif>

		<cfif left(this.storageLocation, 1) NEQ " ">
			<cfset this.subscriptLocation = this.getPiece(this.storageLocation, ";", 1)>
		</cfif>

		<cfset this.dataType = "">

		<cfif isNumeric(left(this.typeString, 1))>
			<cfset this.attributes.multiple = true>

			<cfset this.attributes.subfileNumber = val(this.typeString)>

			<cfset this.datatype = "multiple">

			<cfset this.subfile = createObject("component", "lib.cfmumps.apps.vista.fileMan.file")>
			<cfset this.subfile.open(this.attributes.subfileNumber)>

		<cfelse> <!--- not a multiple --->
			<cfset this.piece = val(this.getPiece(this.storageLocation, ";", 2))>
		</cfif>



		<cfloop from="1" to="#len(this.typeString)#" index="i">
			<cfswitch expression="#mid(this.typeString, i, 1)#">
				<cfcase value="a">
					<cfset this.attributes.auditAlways = true>
				</cfcase>
				<cfcase value="e">
					<cfset this.attributes.auditEditOrDelete = true>
				</cfcase>
				<cfcase value="A">
					<cfset this.attributes.newSubentryVerification = false>
				</cfcase>
				<cfcase value="B">
					<cfset this.attributes.boolean = true>
					<cfset this.dataType = "computed">
				</cfcase>
				<cfcase value="C">
					<cfset this.attributes.computed = true>
					<cfset this.dataType = "computed">
				</cfcase>
				<cfcase value="m">
					<cfset this.attributes.multiLine = true>
				</cfcase>
				<cfcase value="D">
					<cfset this.attributes.dateValued = true>
					<cfset this.dataType = "date">
				</cfcase>
				<cfcase value="F">
					<cfset this.attributes.freeText = true>
					<cfset this.dataType = "string">
				</cfcase>
				<cfcase value="I">
					<cfset this.attributes.immutable = true>
				</cfcase>
				<cfcase value="J">
					<cfset var plStr = mid(this.typeString, i + 1)>
					<cfset this.attributes.printLength = val(plStr)>
				</cfcase>
				<cfcase value="d">
					<cfset this.attributes.printWithDecimals = true>
				</cfcase>
				<cfcase value="K">
					<cfset this.attributes.mCode = true>
					<cfset this.dataType = "mumps">
				</cfcase>
				<cfcase value="M">
					<cfset this.attributes.askAnotherSubentry = true>
				</cfcase>
				<cfcase value="N">
					<cfset this.attributes.numeric = true>
					<cfset this.dataType = "numeric">
				</cfcase>
				<cfcase value="O">
					<cfset this.attributes.hasOutputTransform = true>
				</cfcase>
				<cfcase value="P">
					<cfset this.dataType = "pointer">
					<cfset this.attributes.pointer = true>
					<cfset var ptStr = mid(this.typeString, i + 1)>
					<cfset this.attributes.pointerTo = val(ptStr)>
					<cfset this.pointerTargetGlobalRoot = this.getPiece(fieldDef, "^", 3)>
				</cfcase>
				<cfcase value="'">
					<cfset this.attributes.laygoAllowed = false>
				</cfcase>
				<cfcase value="R">
					<cfset this.attributes.entryRequired = true>
				</cfcase>
				<cfcase value="S">
					<cfset this.dataType = "enum">
					<cfset this.attributes.setOfCodes = true>
					<cfset var setOfCodesData = this.getPiece(fieldDef, "^", 3)>
					<cfset var socArray = listToArray(setOfCodesData, ";", false)>
					<cfset this.codes = {}>

					<cfloop array="#socArray#" index="code">

						<cfset var socKey = this.getPiece(code, ":", 1)>
						<cfset var socValue = this.getPiece(code, ":", 2)>

						<cfset this.codes[socKey] = socValue>
					</cfloop>

				</cfcase>
				<cfcase value="V">
					<cfset this.dataType = "varpointer">
					<cfset this.attributes.variablePointer = true>
				</cfcase>
				<cfcase value="W">
					<cfset this.attributes.wordProcessing = true>
					<cfset this.dataType = "text">
				</cfcase>
				<cfcase value="L">
					<cfset this.attributes.wpLineMode = true>
				</cfcase>
				<cfcase value="X">
					<cfset this.attributes.allowDiModifyEdit = false>
				</cfcase>
				<cfcase value="x">
					<cfset this.attributes.wpWindowProcessing = false>
				</cfcase>
				<cfcase value="*">
					<cfset this.attributes.screenAssociated = true>
				</cfcase>
			</cfswitch>

		</cfloop>

		<cfset db.close()>
		<cfreturn this>
	</cffunction>

	<cffunction name="global" returntype="component" access="public" output="false">
		<cfargument name="IEN" type="numeric" required="true">

		<cfset var glvn = this.file.globalName>
		<cfset var subs = []>
		<cfif this.file.subscripts.len() GT 0>
			<cfset subs.append(this.file.subscripts)>
		</cfif>
		<cfset subs.append(IEN)>
		<cfset subs.append(this.subscriptLocation)>

		<cfset var tmpGlob = createObject("component", "lib.cfmumps.global")>
		<cfset tmpGlob.open(glvn, subs)>

		<cfreturn tmpGlob>
	</cffunction>

	<cffunction name="getValue" returntype="struct" access="public" output="false">
		<cfargument name="IEN" type="any" required="true">

		<cfset var subs = []>
		<cfif this.file.subscripts.len() GT 0>
			<cfset subs.append(this.file.subscripts, true)>
		</cfif>

		<cfset subs.append(arguments.IEN)>

		<cfif this.attributes.multiple>
			<cfset var retVal = {}>
		<cfelse>
			<cfset var retVal = {}>
		</cfif>

		<cfset var dt = this.dataType>
		<cfset var rawValue = "">

		<cfset var retVal.internal = "">
		<cfset var retVal.external = "">

		<cfif dt NEQ "computed" AND dt NEQ "text" AND dt NEQ "varpointer" AND NOT this.attributes.multiple>
			<!--- we can reliably get a raw value from this.subscriptLocation/this.piece --->

			<cfset subs.append(this.subscriptLocation)>
			<cfset var db = createObject("component", "lib.cfmumps.mumps")>
			<cfset db.open()>
			<cfset rawValue = this.getPiece(db.get(this.file.globalName, subs), "^", this.piece)>
			<cfset db.close()>
		</cfif>

		<cfswitch expression="#this.dataType#">
			<cfcase value="date"> <!--- date/time --->
				<cfset retVal.internal = rawValue>
				<cfset retVal.external = convertFromFmDate(rawValue)>
			</cfcase>
			<cfcase value="numeric"> <!--- numeric --->
				<cfset retVal.internal = val(rawValue)>
				<cfset retVal.external = val(rawValue)>
			</cfcase>
			<cfcase value="enum"> <!--- set of codes --->
				<cfif rawValue NEQ "">
					<cfset retVal.external = this.codes[rawValue]>
					<cfset retVal.internal = rawValue>
				</cfif>
			</cfcase>
			<cfcase value="string"> <!--- free text --->
				<cfset retVal.internal = rawValue>
				<cfset retVal.external = rawValue>
			</cfcase>
			<cfcase value="text"> <!--- word processing --->

			</cfcase>
			<cfcase value="computed"> <!--- computed --->

			</cfcase>
			<cfcase value="pointer"> <!--- pointer to a file --->
				<cfset var pointedToFile = createObject("component", "lib.cfmumps.apps.vista.fileMan.file")>
				<cfset pointedToFile.open(this.attributes.pointerTo)>
				<cfset retVal.external = pointedToFile.fields[".01"].getValue(rawValue).external>

				<cfset retVal.internal = pointedToFile.getRecord(rawValue, pointedToFile.fieldNumbers)>

			</cfcase>
			<cfcase value="varpointer"> <!--- variable pointer --->

			</cfcase>
			<cfcase value="mumps"> <!--- MUMPS code --->

			</cfcase>
			<cfcase value="multiple">
				<cfset retVal.external = rawValue>
				<cfset retVal.internal = this.getMultiple(arguments.IEN)>
			</cfcase>
		</cfswitch>

		<cfreturn retVal>
	</cffunction>

	<cffunction name="getMultiple" returntype="struct" access="public" output="false">
		<cfargument name="IEN" type="any" required="true">

		<cfset var subs = duplicate(this.file.storageSubscripts)>
		<cfset subs.append(arguments.IEN)>
		<cfset subs.append(this.fieldNumber)>

		<cfset var g = createObject("lib.cfmumps.global")>
		<cfset g.open(this.file.globalName, subs)>
		<cfset var retVal = g.getObject()>

		<cfset g.close()>

		<cfreturn retVal>
	</cffunction>

</cfcomponent>
