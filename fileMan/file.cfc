<!---
FILEMAN DATA STRUCTURE AND STORAGE LAYOUT

^DIC(50009,"%A")="80^3140710"


--->
<cfcomponent output="false" extends="lib.cfmumps.util">
	<cfset this.ddNumber = "">
	<cfset this.fileName = "">
	<cfset this.globalName = "">
	<cfset this.subscripts = []>
	<cfset this.fileHeader = "">
	<cfset this.mostRecentIEN = "">
	<cfset this.entryCount = "">
	<cfset this.fields = {}>
	<cfset this.fieldNumbers = []>
	<cfset this.modifyDate = "">
	<cfset this.owner = "">

	<cffunction name="open" returntype="component" access="public" output="false">
		<cfargument name="ddNumber" type="numeric" required="true">
		<cfargument name="parentFile" type="component" required="false">
		<cfargument name="parentField" type="component" required="false">

		<cfset var g = createObject("component", "lib.cfmumps.global")>
		<cfset this.ddNumber = arguments.ddNumber>

		<cfif isDefined("arguments.parentFile")>

			<cfset this.fileName = arguments.parentField.fieldLabel>
			<cfset this.globalRoot = arguments.parentFile.globalRoot>
		<cfelse>
			<cfset g.open("DIC", [arguments.ddNumber, 0])>
			<cfset this.fileName = this.getPiece(g.value(), "^", 1)>
			<cfset g.open("DIC", [ddNumber, 0, "GL"])>
			<cfset this.globalRoot = g.value()>
			<cfset g.open("DIC", [ddNumber, "%A"])>
			<cfif g.defined().defined>
				<cfset var fileAttrs = g.value()>
				<cfset this.modifyDate = this.convertFromFmDate(this.getPiece(fileAttrs, "^", 2))>
				<cfset this.ownerDUZ = this.getPiece(fileAttrs, "^", 1)>
				<cfset var ownerFileObj = createObject("component", "lib.cfmumps.apps.vista.fileMan.file")>
				<cfset ownerFileObj.open(200)>
				<cfset this.owner = ownerFileObj.fields[".01"].getValue(this.ownerDUZ)>
			</cfif>
		</cfif>

		<cfset var subscriptList = this.getPiece(this.globalRoot, "(", 2)>
		<cfset this.storageSubscripts = listToArray(subscriptList)>

		<cfset this.globalName = mid(this.globalRoot, 2)>
		<cfset this.globalName = this.getPiece(this.globalName, "(", 1)>

		<cfif find(",", this.globalRoot)>
			<cfset var tmp = this.getPiece(this.globalRoot, "(", 2)>
			<cfset this.subscripts = listToArray(tmp)>
		<cfelse>
			<cfset this.subscripts = []>
		</cfif>


		<cfset var zeroNodeSub = duplicate(this.subscripts)>
		<cfset zeroNodeSub.append(0)>

		<cfset g.open(this.globalName, zeroNodeSub)>
		<cfset this.fileHeader = g.value()>

		<cfset this.mostRecentIEN = this.getPiece(this.fileHeader, "^", 3)>
		<cfset this.entryCount = this.getPiece(this.fileHeader, "^", 4)>

		<cfset var db = createObject("component", "lib.cfmumps.mumps")>
		<cfset db.open()>

		<cfset var lastResult = false>
		<cfset var nextSubscript = "">

		<cfloop condition="lastResult EQ false">
			<cfset var order = db.order("DD", [this.ddNumber, nextSubscript])>
			<cfset lastResult = order.lastResult>
			<cfset nextSubscript = order.value>

			<cfif nextSubscript NEQ "">
				<cfif isNumeric(nextSubscript)>
					<cfif nextSubscript GT 0>
						<cfset var fieldObj = createObject("component", "lib.cfmumps.apps.vista.fileMan.field")>
						<cfset fieldObj.open(this, nextSubscript)>
						<cfset this.fields[nextSubscript] = fieldObj>
						<cfset this.fieldNumbers.append(nextSubscript)>
					</cfif>
				</cfif>
			</cfif>
		</cfloop>
		<cfset db.close()>
		<cfset g.close()>

		<cfreturn this>
	</cffunction>

	<cffunction name="findByIndex" returntype="array" access="public" output="false">
		<cfargument name="searchValue" type="string" required="true">
		<cfargument name="crossReference" type="string" required="true" default="B">

		<cfset var subs = []>
		<cfif this.subscripts.len() GT 0>
			<cfset subs.append(this.subscripts, true)>
		</cfif>

		<cfset subs.append(arguments.crossReference)>
		<cfset subs.append(arguments.searchValue)>

		<cfset var db = createObject("component", "lib.cfmumps.mumps")>
		<cfset db.open()>

		<cfif db.data(this.globalName, subs).hasSubscripts>
			<cfset retVal = []>

			<cfset var lastResult = false>
			<cfset var nextSubscript = "">
			<cfset subs.append(nextSubscript)>

			<cfloop condition="lastResult EQ false">
				<cfset var order = db.order(this.globalName, subs)>
				<cfset lastResult = order.lastResult>
				<cfset nextSubscript = order.value>
				<cfset subs[subs.len()] = nextSubscript>

				<cfif order.value NEQ "">
					<cfset retVal.append(order.value)>
				</cfif>
			</cfloop>
		<cfelse>
			<cfset retVal = []>
		</cfif>

		<cfset db.close()>
		<cfreturn retVal>
	</cffunction>

	<cffunction name="getAllEntryNumbers" returntype="array" access="public" output="false">

		<cfset var oa = []>

		<cfset var lastResult = false>

		<cfset var subs = duplicate(this.subscripts)>
		<cfset subs.append("")>

		<cfset db = createObject("component", "lib.cfmumps.mumps")>
		<cfset db.open()>

		<cfloop condition="lastResult EQ false">
			<cfset var order = db.order(this.globalName, subs)>
			<cfset lastResult = order.lastResult>
			<cfset subs[subs.len()] = order.value>

			<cfif order.value NEQ "" AND isNumeric(order.value) AND order.value GT 0>
				<cfset oa.append(order.value)>
			</cfif>
		</cfloop>

		<cfset db.close()>
		<cfreturn oa>
	</cffunction>


	<cffunction name="getRecords" returntype="array" access="public" output="false">
		<cfargument name="entryNumbers" type="array" required="true">
		<cfargument name="fieldNumbers" type="array" required="true">

		<cfset var oa = []>

		<cfloop array="#arguments.entryNumbers#" index="ien">
			<cfset oa.append(this.getRecord(ien, arguments.fieldNumbers))>
		</cfloop>

		<cfreturn oa>
	</cffunction>

	<cffunction name="getRecord" returntype="struct" access="public" output="false">
		<cfargument name="IEN" type="any" required="true">
		<cfargument name="fieldNumbers" type="array" required="true">

		<cfset var os = {}>

		<cfloop array="#arguments.fieldNumbers#" index="fieldNum">
			<cfset os[fieldNum].IEN = arguments.IEN>
			<cfset os[fieldNum].label = this.fields[fieldNum].fieldLabel>
			<cfset os[fieldNum].type = this.fields[fieldNum].dataType>
			<cfset var tVal = this.fields[fieldNum].getValue(arguments.IEN)>
			<cfset os[fieldNum].externalValue = tVal.external>
			<cfset os[fieldNum].internalValue = tVal.internal>
			<cfset os[fieldNum].multiple = this.fields[fieldNum].attributes.multiple>
		</cfloop>

		<cfreturn os>
	</cffunction>

	<cffunction name="getQuery" returntype="query" access="public" output="false">
		<cfargument name="records" type="array" required="true">

		<cfset var cols = []>
		<cfset var fieldNumbers = listToArray(structKeyList(arguments.records[arguments.records.len()]))>

		<cfloop array="#fieldNumbers#" index="fieldNum">
			<cfset cols.append(this.fields[fieldNum].fieldLabel)>
		</cfloop>
		<cfset cols.append("__fmInternalEntryNumber")>

		<cfset tQry = queryNew(arrayToList(cols))>
		<cfset queryAddRow(tQry, records.len())>

		<cfset var rowNum = 1>

		<cfloop array="#arguments.records#" index="record">
			<cfloop array="#fieldNumbers#" index="fieldNum">
				<cfset var colName = this.fields[fieldNum].fieldLabel>
				<cfset querySetCell(tQry, colName, record[fieldNum].externalValue, rowNum)>
				<cfset querySetCell(tQry, "__fmInternalEntryNumber", record[fieldNum].IEN, rowNum)>
			</cfloop>
			<cfset rowNum = rowNum + 1>
		</cfloop>
		<cfreturn tQry>
	</cffunction>

</cfcomponent>
